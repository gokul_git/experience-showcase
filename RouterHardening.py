#!/usr/bin/env python

## Task : Hardening Ver 1.1
## Created By : Dinesh Balaraman
## Created On : 01-FEB-2014
## Last Modified By : Dinesh Balaraman
## Last Modified On : 26-SEP-2014
## Number of times modified : 6

import pexpect,time
import re

llYN_PWD = ['yes/no','assword:']
ll=['#','$']
output, lAccessList = [], []

ip="""@@ip_address@@""".strip()
uname="""@@wf_user_name@@""".strip()
pwd="""@@wf_password@@""".strip()

#prompt="""wf_prompt""".strip()  # commented on JUN-04-2014

prompt = "#.*$"

#if prompt.find("$") >= 0:          # commented on JUN-04-2014
#  prompt=prompt.replace("$","\$")   # commented on JUN-04-2014


#if len(prompt.strip(" ")) > 0:             # commented on JUN-04-2014
#  global_prompt=prompt[:-1] + "\(config\)" + prompt[-1]
#  glocal_line_prompt=prompt[:-1] + "\(config-line\)" + prompt[-1]
#  global_if_prompt=prompt[:-1] + "\(config-if\)" + prompt[-1]
#  global_subif_prompt=prompt[:-1] + "\(config-subif\)" + prompt[-1]
#  global_ext_prompt=prompt[:-1] + "\(config-ext-nacl\)" + prompt[-1]

## Latest added on JUN-04-2014

global_prompt="#.*$"
glocal_line_prompt="#.*$"
global_if_prompt="#.*$"
global_subif_prompt="#.*$"
global_ext_prompt="#.*$"
  

def Delimiter(sStr):
  if sStr.find("\r\n") >= 0:
    return "\r\n"
  else:
    return "\n"
  
def InverseSubNetMask(sPara):
  if len(sPara) > 0:
    if sPara:
      sSubNetMask=sPara
      if sSubNetMask:
        return ".".join(str(255-int(i)) for i in sSubNetMask.strip().split("."))

def BinaryBitArray(Val1,Val2):
  AndDict = {"00":"0","01":"0","10":"0","11":"1"}
  returnVal = ""
  if len(Val1) > len(Val2):
    Val2 = "".join(str(0) for i in range(0,(len(Val1)-len(Val2)))) + Val2
  else:
    Val1 = "".join(str(0) for i in range(0,(len(Val2)-len(Val1)))) + Val1
  for i in range(0,len(Val1)):
    returnVal += AndDict[Val1[i] + Val2[i]]
  return returnVal

def IP2Binary(IPAddress):
  retBinary = ""
  for eachOctal in IPAddress.strip().split("."):
    retBinary += "".join(str(0) for i in range(0,(8-len(format(int(eachOctal),'08b'))))) + format(int(eachOctal),'08b')
  return retBinary

def Binary2IP(BinaryValue):
  retIPAddress = ""
  BinaryValue = "".join(str(0) for i in range(0,(32-len(BinaryValue)))) + BinaryValue
  for i in range(0,len(BinaryValue),8):
    retIPAddress += "%s" % int(BinaryValue[i:i+8],base=2) + "."
  return retIPAddress[:-1]

def ExecuteAndStore(cmd,expectStr,executeExpect="YES",delay=0):  
  p.sendline(cmd)
  if delay > 0:
    time.sleep(delay)
  if executeExpect == "YES":
    p.expect(['Permission denied', 'Terminal type'] + expectStr ,100) 
  #print cmd + " Done"

def Store(cmd,expectStr,executeExpect="YES",delay=0): 
  p.sendline(cmd)
  if delay > 0:
    time.sleep(delay)
  if executeExpect == "YES":
    p.expect(['Permission denied', 'Terminal type'] + expectStr ,100) 
    output.append(p.before.split("\r\n"))

def handleMore():
    app=""
    more=0
    while True:
        out=0
        try:
            if more == 0:
              time.sleep(3)
            out = p.expect(["--More--",'--More-- ','--more--','--more-- ',prompt])            
            more += 1
            app += p.before
        except Exception as e:
            out=256
        if out==256:
            break
        elif out>3:           
            output.append(app.split("\r\n"))
            break        
        p.send('\n')
## telnet option has been added in V 1.1

telnet_path = "@@telnet@@"
tacacs_usr = "@@tacacs_user_name@@"
tacacs_pwd = "@@tacacs_password@@"
p=""
ssh=0

try:
  try:
      ## SSH Part
      p = pexpect.spawn("ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -l " + uname + " " + ip)
      i = p.expect(['sername','assword','yes/no'])
      if i == 2:
        p.sendline('yes')
        p.expect('assword:')
        p.sendline(pwd)
      elif i == 1:
        p.sendline(pwd)
      ssh = 1
  except Exception as e:
      ## SSH Not Enabled
      ## TELNET PART
      if (str(e).lower().find("connection refused") >= 0) or (1==1):
        try:
          p = pexpect.spawn(telnet_path + " " + ip + " 23")
          ## First time sending Tacacs Id
          i = p.expect(['(?i)USERNAME','(?i)PASSWORD','yes/no'])
          if i == 2:
            p.sendline('yes')
            p.expect('(?i)PASSWORD')
            p.sendline(pwd)
          elif i == 1:
            p.sendline(pwd)
          elif i == 0:
            p.sendline(uname)
            p.expect('(?i)PASSWORD')
            p.sendline(pwd)
          ## Second time sending local id
          i = p.expect(['(?i)USERNAME','(?i)PASSWORD','yes/no',prompt])
          if i != 3:
            if i == 2:
              p.sendline('yes')
              p.expect('(?i)PASSWORD')
              p.sendline(tacacs_pwd)
            elif i == 1:
              p.sendline(tacacs_pwd)
            elif i == 0:
              p.sendline(tacacs_usr)
              p.expect('(?i)PASSWORD')
              p.sendline(tacacs_pwd)
            i = p.expect(['(?i)USERNAME','(?i)PASSWORD','yes/no',prompt])
            if i != 3:
              output.append(["Error","Authentication Failed."])
              print output
              exit(0)
        except Exception as e:
          ## TELNET Not Enabled
          temp=[]
          temp.append("Error")
          if ip.strip(" ") == "":
            temp.append("This router is not added under hardening asset list. Please contact Administrator")
          else:
            for line in str(e).split("\n"):
              if line.lower().find("before ") >= 0:
                temp.append(line[len("before "):])
            output.append(temp)
            print output
            exit(0)
    
  if (ssh == 1):
    auth=p.expect(['Permission denied', '(?i)USERNAME', 'uthentication failed!' ,'assword:', 'Terminal type', prompt],100)
    if (auth == 1) or (auth == 2) or (auth == 3):
      output.append(["Error","Authentication Failed."])
      print output
      exit(0)
    
  ## Take backup of the router before hardening
  ExecuteAndStore("terminal length 0",[prompt])
  Store("show running-config",[prompt],"YES",5)
  target=open("/tmp/Before_Hardening@@AZ_RUN_ID@@.txt","w")
  target.writelines("\n".join(output[-1]))
  del output[-1]
  target.close()
    
  ## Pre-Configuration chklist
  ExecuteAndStore("sh ip int brief | e unassigned",[prompt])
  
  ## Get all the interfaces into a list
  ptoutput=p.before.strip().split(Delimiter(p.before))[1:-1]
  interfaces=[]
  for i in ptoutput:
    bAccessList = False
    sDelimit = ""
    if i.lower().strip().startswith("interface ") == True:
      continue
    else:
      ExecuteAndStore("sh run int " + i.strip().split(" ")[0],[prompt])
      for eachLine in p.before.strip().split(Delimiter(p.before))[1:-1]:
        if eachLine.lower().find("description") >= 0:
          if eachLine.lower().find("lan") >= 0:
            bAccessList = True
            interfaces.append("l" + i.strip().split(" ")[0])            
          else:
            interfaces.append("w" + i.strip().split(" ")[0])
      if bAccessList == True:
        for eachLine in p.before.strip().split(Delimiter(p.before))[1:-1]:
          if eachLine.lower().find("ip address") >= 0:
            out = " ".join(re.findall('[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}',eachLine)).strip() ## Added on 07-JUN-14
            if out:
              lAccessList.append(out)
            #lAccessList.append(eachLine[eachLine.find("ip address") + 10:].strip())
  ##print lAccessList

  for i in interfaces:
    ExecuteAndStore("sh run int " + i.strip(" ")[1:],[prompt])  
    temp=[]
    temp.append(i.strip(" ")[1:])
    for i in p.before.split(Delimiter(p.before))[1:-1]:
      temp.append(i)
    output.append(temp)
        
  Store("sh run | i security authentication failure rate 3 log",[prompt])
  Store("sh run | i login block-for 100 attempts 3 within 100",[prompt])
  Store("sh run | i login delay 10",[prompt])
  Store("sh run | i enable secret",[prompt])
  Store("sh ip ssh",[prompt])
  Store("sh run | i no",[prompt],"YES",5)  
  Store("sh run | s line vty",[prompt])
  Store("sh run | i session",[prompt])
  Store("sh run | i clock",[prompt])
  Store("sh run | i logging",[prompt])
  Store("sh run | i service",[prompt])
  Store("sh run | i log",[prompt])
  Store("sh run | b banner",[prompt],"YES",5)
  Store("sh run | i 192.168.40.215",[prompt]) # Added on 23-Dec-2014
  Store("sh run | i 202.177.130.73",[prompt]) # Added on 23-Dec-2014
  output.append("--------------------------Here comes the after hardening part ------------------------")
  ##Router Hardenig starts here...
  #print "Entering into global config mode"
  ExecuteAndStore("conf t",[global_prompt])
  #print "In G Mode"
  #print "Hardening started#################################################################################"
  ExecuteAndStore("security authentication failure rate 3 log",[global_prompt])
  ExecuteAndStore("login block-for 100 attempts 3 within 100",[global_prompt])
  ExecuteAndStore("login delay 10",[global_prompt])
  ExecuteAndStore("enable secret a$!@np@!nts",[global_prompt])
  ExecuteAndStore("ip domain name asianpaints.com",[global_prompt])  ## added in V 1.1
  ExecuteAndStore("crypto key generate rsa",[llYN_PWD[0]] )
  ExecuteAndStore("yes",["many bits"])
  ExecuteAndStore("1024",[global_prompt],"YES",10)
  ExecuteAndStore("ip ssh time-out 120",[global_prompt]) ## timeout -> time-out
  ExecuteAndStore("ip ssh authentication-retries 3",[global_prompt])
  ExecuteAndStore("ip ssh version 2",[global_prompt])
  ExecuteAndStore("no service config",[global_prompt])
  #p.sendline('no service finger/no finger')   ## cmd not working
  ExecuteAndStore("no ip source-route",[global_prompt])
  #p.sendline('no ftp-server enable') ## cmd not working
  ExecuteAndStore("no service tcp-small-servers",[global_prompt])
  ExecuteAndStore("no service udp-small-servers",[global_prompt])
  ExecuteAndStore("no ip bootp server",[global_prompt])
  ExecuteAndStore("no ip identd",[global_prompt])
  ExecuteAndStore("no snmp-server community private RW",[global_prompt]) ##Cannot find community private 
  ExecuteAndStore("no snmp-server community public RO",[global_prompt])  ##Cannot find community public 
  ExecuteAndStore("no service dhcp",[global_prompt])
  ExecuteAndStore("no service pad",[global_prompt])
  ExecuteAndStore("no boot network",[global_prompt])
  ExecuteAndStore("no ip domain lookup",[global_prompt])
  ExecuteAndStore("no ip http server",[global_prompt])
  ExecuteAndStore("no ip http secure-server",[global_prompt])
  ExecuteAndStore("line vty 0 4",[glocal_line_prompt])
  ExecuteAndStore("transport input ssh",[glocal_line_prompt])
  ExecuteAndStore("transport output none",[glocal_line_prompt])
  ExecuteAndStore("session-timeout 5",[glocal_line_prompt])
  ExecuteAndStore("line vty 5 15",[glocal_line_prompt])
  ExecuteAndStore("transport input ssh",[glocal_line_prompt])
  ExecuteAndStore("transport output none",[glocal_line_prompt])
  ExecuteAndStore("session-timeout 5",[glocal_line_prompt])
  ExecuteAndStore("Line Con 0",[glocal_line_prompt])
  ExecuteAndStore("session-timeout 5",[glocal_line_prompt])
  ExecuteAndStore("line aux 0",[glocal_line_prompt])                  # Added in Ver4
  ExecuteAndStore("no exec",[glocal_line_prompt])                     # Added in Ver4
  ExecuteAndStore("transport input none",[glocal_line_prompt])        # Added in Ver4
  ExecuteAndStore("service password-encryption",[global_prompt])      # Added in Ver4
  ExecuteAndStore("clock timezone IST 5 30",[global_prompt])
  ExecuteAndStore("logging console critical",[global_prompt])
  ExecuteAndStore("logging trap critical",[global_prompt])
  ExecuteAndStore("service tcp-keepalives-in",[global_prompt])
  ExecuteAndStore("service tcp-keepalives-out",[global_prompt])
  ExecuteAndStore("service timestamps debug datetime localtime show-timezone msec",[global_prompt])
  ExecuteAndStore("service timestamps log datetime localtime show-timezone msec",[global_prompt])
  ExecuteAndStore("service password-encryption",[global_prompt])
  ExecuteAndStore("logging buffered 4096",[global_prompt])  
  ExecuteAndStore("no banner motd",[global_prompt])
  ExecuteAndStore("no banner login",[global_prompt])
  ExecuteAndStore("ip access-list extended 135",[global_prompt]) # added on 26-SEP-2014
  ExecuteAndStore("5 permit ip host 192.168.40.53 any",[global_prompt]) # added on 26-SEP-2014
  ExecuteAndStore("banner login ^C",[""],"NO")
  ExecuteAndStore("""
***********************************************************************************
* Warning:   This system is property of ************ & access to System is being  *
* Monitored, Unauthorized attempt to access will be considered as a violation &   *
* will attract legal action.                                                      *
* CONTACT DETAILS: Phone: ********************                                    *
*                  Toll Free: *************                                       *
*                             *************                                       *
*                  C i s c o  S y s t e m s                                       *
*                  ************   Maintained By *****.                            *
***********************************************************************************^C""",[global_prompt])
  
  ExecuteAndStore("exit",[prompt])
  
    
    #print "Configuring Interfaces #################################################################################"
    
  ExecuteAndStore("conf t",[global_prompt])        
  for i in interfaces:  
      if i.strip(" ").startswith("l"):    
        #print "Configuring LAN #################################################################################"
        ExecuteAndStore("int " + i.strip(" ")[1:],[global_if_prompt,global_subif_prompt])
        ExecuteAndStore("no ip directed-broadcast",[global_if_prompt,global_subif_prompt])
        ExecuteAndStore("no ip proxy-arp",[global_if_prompt,global_subif_prompt])
        ExecuteAndStore("no ip unreachable",[global_if_prompt,global_subif_prompt])
        ExecuteAndStore("no mop enabled",[global_if_prompt,global_subif_prompt])
        ExecuteAndStore("no ip mask-reply",[global_if_prompt,global_subif_prompt])
        ExecuteAndStore("no ip redirect",[global_if_prompt,global_subif_prompt])
        ExecuteAndStore("ip tcp adjust-mss 1400",[global_if_prompt,global_subif_prompt]) ##ip tcp-adjust mss 1400 -> ip tcp adjust-mss 1400
        ExecuteAndStore("exit",[global_prompt])
      elif i.strip(" ").startswith("w"):
        #print "Configuring WAN #################################################################################"
        ExecuteAndStore("int " + i.strip(" ")[1:],[global_if_prompt,global_subif_prompt])  ## Changed from global_subif_prompt to global_if_prompt because of the server 172.29.55.233
        ExecuteAndStore("no ip directed-broadcast",[global_if_prompt,global_subif_prompt])
        ExecuteAndStore("no ip proxy-arp",[global_if_prompt,global_subif_prompt])
        ExecuteAndStore("no ip unreachable",[global_if_prompt,global_subif_prompt])
        ExecuteAndStore("no mop enabled",[global_if_prompt,global_subif_prompt])
        ExecuteAndStore("no ip mask-reply",[global_if_prompt,global_subif_prompt])
        ExecuteAndStore("no ip redirect",[global_if_prompt,global_subif_prompt])
        ExecuteAndStore("no cdp enable",[global_if_prompt,global_subif_prompt])
        ExecuteAndStore("exit",[global_prompt])
        
  ############################################  Anti -proof included in version 4 ##################################
  
  #if len(lAccessList) > 0:
  #  ExecuteAndStore("ip access-list ex anti-spoof",[global_ext_prompt])
  #  Branch_IP=Binary2IP(BinaryBitArray(IP2Binary(lAccessList[0].strip().split(" ")[0]),IP2Binary(lAccessList[0].strip().split(" ")[1])))
  #  Branch_SubNetMask=InverseSubNetMask(lAccessList[0].strip().split(" ")[1])
  #  ExecuteAndStore("deny ip " + Branch_IP + " " + Branch_SubNetMask + " any",[global_ext_prompt])
  #  ExecuteAndStore("deny ip host 0.0.0.0 any log",[global_ext_prompt])
  #  ExecuteAndStore("deny ip 127.0.0.0 0.255.255.255 any log",[global_ext_prompt])
  #  ExecuteAndStore("deny ip 224.0.0.0 0.255.255.255 any log",[global_ext_prompt])
  #  ExecuteAndStore("permit ip any any",[global_ext_prompt])
  #  ExecuteAndStore("exit",[global_prompt])
  
  ExecuteAndStore("ip access-list ex anti-spoof",[global_ext_prompt])
  if len(lAccessList) > 0:
    Branch_IP=Binary2IP(BinaryBitArray(IP2Binary(lAccessList[0].strip().split(" ")[0]),IP2Binary(lAccessList[0].strip().split(" ")[1])))
    Branch_SubNetMask=InverseSubNetMask(lAccessList[0].strip().split(" ")[1])
    ExecuteAndStore("deny ip " + Branch_IP + " " + Branch_SubNetMask + " any",[global_ext_prompt])
  ExecuteAndStore("deny ip host 0.0.0.0 any log",[global_ext_prompt])
  ExecuteAndStore("deny ip 127.0.0.0 0.255.255.255 any log",[global_ext_prompt])
  ExecuteAndStore("deny ip 224.0.0.0 0.255.255.255 any log",[global_ext_prompt])
  ExecuteAndStore("permit ip any any",[global_ext_prompt])
  ExecuteAndStore("exit",[global_prompt])
  
#  ExecuteAndStore("exit",[prompt])
  
  
  ##################################   code is for applying anti-proof -  added in version 4 #################################
  
  for i in interfaces:
    if i.startswith("w") == True:
      ExecuteAndStore("int " + i.strip(" ")[1:],[global_if_prompt,global_subif_prompt])
      ExecuteAndStore("ip access-group anti-spoof in",[global_if_prompt,global_subif_prompt])
      ExecuteAndStore("exit",[global_prompt])
      
  #ExecuteAndStore("ip access-list extended TRAFFIC_IN",[global_prompt])   ##added in v1.1
  #ExecuteAndStore("19 permit ip any host 202.177.130.73",[global_prompt]) ##added in v1.1  
  #ExecuteAndStore("exit",[global_prompt])
  
  # Add access list - 23-DEC-2014
  ExecuteAndStore("ip access-list extended 135",[global_prompt])
  ExecuteAndStore("45 Permit ip host 192.168.40.215 any",[global_prompt])
  ExecuteAndStore("exit",[global_prompt])
  # Add access list - 23-DEC-2014
  ExecuteAndStore("ip access-list extended TRAFFIC_IN",[global_prompt])
  ExecuteAndStore("no permit ip any host 202.177.130.73",[global_prompt])
  ExecuteAndStore("exit",[global_prompt])
  
  
  
  ExecuteAndStore("exit",[prompt])
  
  ExecuteAndStore("write",[prompt],"YES",10)     ## This is where the entire configurations gets permanently written in Router
  
  
  for i in interfaces:
    ExecuteAndStore("sh run int " + i.strip(" ")[1:],[prompt])  
    temp=[]
    temp.append(i.strip(" ")[1:])
    for i in p.before.split(Delimiter(p.before))[1:]:
      temp.append(i)
    output.append(temp)
    
  ## Post-Configuration chklist  
  Store("sh run | i security authentication failure rate 3 log",[prompt])
  Store("sh run | i login block-for 100 attempts 3 within 100",[prompt])
  Store("sh run | i login delay 10",[prompt])
  Store("sh run | i enable secret",[prompt])
  Store("sh ip ssh",[prompt])
  Store("sh run | i no",[prompt],"YES",5)
  Store("sh run | s line vty",[prompt])
  Store("sh run | i session",[prompt])
  Store("sh run | i clock",[prompt])
  Store("sh run | i logging",[prompt])
  Store("sh run | i service",[prompt])
  Store("sh run | i log",[prompt])
  Store("sh run | b banner",[prompt],"YES",5)
  Store("sh run | i 192.168.40.215",[prompt]) # Added on 23-Dec-2014
  Store("sh run | i 202.177.130.73",[prompt]) # Added on 23-Dec-2014
  ## Take backup of the router after hardening
  Store("show running-config",[prompt],"YES",5)
  target=open("/tmp/After_Hardening@@AZ_RUN_ID@@.txt","w")
  target.writelines("\n".join(output[-1]))
  del output[-1]
  target.close()
  
  ExecuteAndStore("exit",[""],"NO")
  print output
  
  print output
except Exception as e:
  temp=[]
  temp.append("Error")
  if ip.strip(" ") == "":
    temp.append("This router is not added under hardening asset list. Please contact Administrator")
  else:
    for line in str(e).split("\n"):    
      if line.lower().find("before ") >= 0:
        temp.append(line[len("before "):])
  output.append(temp)
  print output