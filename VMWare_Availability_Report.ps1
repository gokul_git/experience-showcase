$ErrorActionPreference = "SilentlyContinue"
Add-PSSnapin VMware.VimAutomation.Core

Function Get-Avail-Details{
Param(
[cmdletbinding()]
[Datetime]$DayStart,
[Datetime]$DayEnd )

	$DictAvail = @{}
	$VMArray = Get-VM | where {$_.powerstate -eq "PoweredON"}
	Write-Host $DayStart
	Write-Host $DayEnd
	$ccccnt = $VMArray.Count
	$ichk = 0
	foreach($EachVM in $VMArray){
		$ichk += 1
		Write-Host "$ichk of $ccccnt"
		$collection = Get-Stat -Entity $EachVM  -Stat "sys.osUptime.latest" -Start $DayStart -Finish $DayEnd -ErrorAction SilentlyContinue | Group-Object -Property {$_.Entity.Name}
		if($collection.Group){
			Write-Host $EachVM
			Write-Host $Groups.Count - 1
			$Groups = $collection.Group
			$GCount = $Groups.Count - 1
			$IntervalSecs = $Groups[0].IntervalSecs
			$DownTimeValues = New-Object System.Collections.ArrayList
			$DownTimeDiffs = New-Object System.Collections.ArrayList

			for($i=$GCount; $i -ge 0; $i--){
				$DownTimeValues.Add($Groups[$i]) | Out-Null
			}

			for($i=0; $i -lt ($DownTimeValues.Count - 1); $i++){
				$Diff = $DownTimeValues[$i+1].Value - $DownTimeValues[$i].Value
                #if((($Diff - 1) - $IntervalSecs) -lt 0){
				if((($Diff) - $IntervalSecs) -lt 0){
					$SecDiff = $DownTimeValues[$i+1].Timestamp - $DownTimeValues[$i].Timestamp
					$Down = $SecDiff.TotalSeconds - $DownTimeValues[$i+1].Value
					$DownTimeDiffs.Add($Down) | Out-Null
				}else{
					$DownTimeDiffs.Add(0) | Out-Null
				}
			}

			$TotalSecs = 0
			#The below snippet handles if there is no statistic data @ date start and @ end
			if(($DownTimeValues[0].Timestamp - $DayStart).TotalSeconds -gt 0 ){
				$TotalSecs += ($DownTimeValues[0].Timestamp - $DayStart).TotalSeconds
			}
			if(($DayEnd - $DownTimeValues[$GCount].Timestamp).TotalSeconds -gt 0){
				$TotalSecs += ($DayEnd - $DownTimeValues[$GCount].Timestamp).TotalSeconds
			}


			for($j=0; $j -lt $DownTimeDiffs.Count; $j++){
				if($DownTimeDiffs[$j] -gt 0){
					$TotalSecs += $DownTimeDiffs[$j]
				}
			}

			#$DurationSeconds = ($DownTimeValues[$GCount].Timestamp - $DownTimeValues[0].Timestamp).TotalSeconds
            $DurationSeconds = 86400
			$Result = 100 - (($TotalSecs/$DurationSeconds)*100)
			$DictAvail.Add($EachVM.Name, $Result)
			Write-Host "$EachVM ------------- $Result"
		}else{
			$DictAvail.Add($EachVM.Name, 0)
		}
	}

	return $DictAvail
}


Connect-VIServer 192.168.107.130

$ActualDayStart = ([datetime]::Today).AddDays(-1)
$ActualDayEnd = [datetime]::Today
$ActualDayDiff = ($ActualDayEnd - $ActualDayStart).TotalDays
$ALCollectDicts = New-Object System.Collections.ArrayList
$DictConsolidated = @{}
$File = "D:\DD\VMAvailabilityReport\MSAP\Daily\VMwareAvailabilityReport_WSD_Daily_" + (Get-Date $ActualDayStart -Format ddMMyyyyhh) + ".csv"



if(($ActualDayEnd - $ActualDayStart).TotalDays -gt 0){
	for($i=0; $i -lt (($ActualDayEnd - $ActualDayStart).TotalDays); $i++){
		$RetDict = Get-Avail-Details -DayStart $ActualDayStart.AddDays($i) -DayEnd $ActualDayStart.AddDays($i+1)
		$ALCollectDicts.Add($RetDict)
		Write-Host "-------"
	}
}

foreach($dic in $ALCollectDicts){
	foreach($key in $dic.Keys){
		if($DictConsolidated.ContainsKey($key)){
			$DictConsolidated[$key] += $dic[$key]
		}else{
			$DictConsolidated.Add($key, $dic[$key])
		}
	}
}

$ResultCSV = @()
foreach($key in $DictConsolidated.Keys){
	$Extradetails = Get-VM $key | Select Name, CustomFields, Host, @{N="IP Address";E={@($_.guest.IPAddress[0])}}
	$DNSName = (Get-VM $key).Guest.HostName
	$ResultCSV += New-Object PSObject -Property @{
		From_Date = $ActualDayStart
		To_Date = $ActualDayEnd
		VM_Name = $key
		IPAddress = $Extradetails."IP Address"
		Uptime = "{0:N2}" -f ($DictConsolidated[$key] / $ActualDayDiff)
		Unit = "Percentage(%)"
		DNS_Name = $DNSName
		ApplicationTierOfVM = $Extradetails.CustomFields['Application Tier']
	}
}

$ResultCSV | Select From_Date, To_Date, VM_Name, IPAddress, Uptime, Unit, DNS_Name, ApplicationTierOfVM | Export-Csv -Path $File -NoTypeInformation -UseCulture

#Send Email with Availability Report
$Subject = "PROD-Site : Daily VM Availability Report for Sime Darby (WSD)~" + (Get-Date -Format ddMMyyyyhh)
$From = "MSAPAutomation@xxxx.com"
$To = @("DCARV.Admin.my@xxxx.com")
#$Attach = "D:\DD\VMAvailabilityReport\Scheduler\Daily\VMwareAvailabilityReport_WSD_Daily_" + (Get-Date -Format ddMMyyyyhh) + ".csv"
$Body = @"
Hi All,

Please find the attached daily availability report for  ~ PROD site(WSD)


---
 Automation Team

"@

Send-MailMessage -From $From -To $To -Subject $Subject -Body $Body -Attachments $File -SmtpServer 192.228.220.72
