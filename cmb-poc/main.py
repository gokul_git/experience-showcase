"""This script gets the cluster health status from vROPS"""
from getpass import getpass
import logging
import json
import sys
import getopt
from nagini import Nagini

logging.basicConfig()
LOGGER = logging.getLogger('LOGGER')
LOGGER.setLevel(logging.INFO)

def log_event(event):
    """Logs event information for debugging"""
    LOGGER.info("====================================================")
    LOGGER.info("\n" + event)
    LOGGER.info("====================================================")

def ppjson(dictionary):
    """Print dictionary as pretty JSON"""
    if isinstance(dictionary, dict):
        return json.dumps(dictionary, indent=4, separators=(',', ': '))
    else:
        return dictionary

def prompt_password(username):
    """Get password"""
    password = getpass("Type the {}'s password securely: ".format(username))
    return password

def print_help(exit_code=0):
    """Print how to use this script and exit"""
    print 'main.py -o <vrops_name> -u <username> [-p <password>]'
    sys.exit(exit_code)

def vrops_client(hostname, username="admin", password=""):
    """Get authenticated with vROPS client"""
    if not password:
        password = prompt_password(username)
    vrops = Nagini(host=hostname, user_pass=(username, password))
    return vrops

def check_adapter_exist(client, adapter, resource=None):
    """Check is the given Adapter(/Resource) installed in vROPS"""
    adapter_types = client.get_adapter_types()
    adapters = [name["name"] for name in adapter_types["adapter-kind"]]
    if (adapter in adapters) and resource:
        res = client.get_resources_with_adapter_kind(adapterKindKey="VMWARE")
        reslist = [name['resourceKey']['resourceKindKey'] for name in res['resourceList']]
        return bool(resource in reslist)
    else:
        return False

def get_resources_adapter(client, adapterkind=None, resourcekind=None):
    """Get vCenter resource status from vROPS API"""
    try:
        if not adapterkind and not resourcekind:
            return client.get_resources()
        elif not resourcekind:
            if check_adapter_exist(client, adapterkind):
                return client.get_resources(adapterKind=adapterkind)
            else:
                LOGGER.error("No Adapter found - %s", adapterkind)
        else:
            if check_adapter_exist(client, adapterkind, resourcekind):
                return client.get_resources(adapterKind=adapterkind, resourceKind=resourcekind)
            else:
                LOGGER.error("No Adapter found - %s", adapterkind)
    except Exception as err:
        LOGGER.error("Failed to get the vROPS resources: %s", err)

def main(argv):
    """Main function"""
    hostname = ""
    username = ""
    password = ""
    try:
        opts, args = getopt.getopt(argv, "ho:u:p:",
                                   ["hostname=", "username=", "password="])
    except getopt.GetoptError:
        print_help(2)
    for opt, arg in opts:
        if opt == '-h':
            print_help()
        elif opt in ("-o", "--hostname"):
            hostname = arg
        elif opt in ("-u", "--username"):
            username = arg
        elif opt in ("-p", "--password"):
            password = arg
        else:
            LOGGER.error("Unknown option")
            print_help(2)
    if not hostname or not username:
        print_help(2)
    client = vrops_client(hostname, username, password)
    log_event(ppjson(get_resources_adapter(client)))
    log_event(ppjson(get_resources_adapter(client, "VMWARE")))
    log_event(ppjson(get_resources_adapter(client, "VirtualAndPhysicalSANAdapter")))

if __name__ == "__main__":
    main(sys.argv[1:])
